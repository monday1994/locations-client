import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import MainMapComponent from './MapComponents/Main';

class App extends Component {
  render() {
    return (
      <div className="App">
        <p className="App-intro">
          <MainMapComponent />
        </p>
      </div>
    );
  }
}

export default App;
