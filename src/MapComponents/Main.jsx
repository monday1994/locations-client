import React, {Component} from 'react';
import GoogleMapReact from 'google-map-react';
import { getLocationsInRange } from '../API/locationsApi';
import MyGreatPlace from './my_great_place.jsx';

const apiKey = 'AIzaSyBhy-78Y_H0Axa6bvFbl3xiPFg8jrprHmU';


export default class Main extends Component {

    constructor(props){
       super(props);
       this.state = {
         identifier: 'Hy4wOVwRG',
         startDate: '2018-05-14T16:30:53.487Z',
         endDate: '2018-05-14T19:30:53.487Z',
         locations: []
       }

       this.handleInput = this.handleInput.bind(this);
       this.handleStartDate = this.handleStartDate.bind(this);
       this.handleEndDate = this.handleEndDate.bind(this);
       this.setPoints = this.setPoints.bind(this);
       this.renderPoints = this.renderPoints.bind(this);
    }

    static defaultProps = {
        center: {
          lat: 50.07,
          lng: 19.98
        },
        zoom: 11
      };

      setLocations(identifier, startDate, endDate){
        getLocationsInRange(identifier, startDate, endDate).then(responseData => {
          const responseInJSON = responseData.json();

          responseInJSON.then((rawResponseData => {
              this.setState({locations: rawResponseData.result.locations}, () => {
                console.log("state aft  = ", this.state);
              });
          }));             
      });
      }

      componentDidMount(){
          console.log("did mount");
          this.setLocations('Hy4wOVwRG', '2018-05-14T16:30:53.487Z', '2018-05-14T19:30:53.487Z');
      }

      handleInput(e){
        console.log("identifier = ", e.target.value);
        this.setState({identifier: e.target.value});
      }

      handleStartDate(e){
        this.setState({startDate: e.target.value});
      }

      handleEndDate(e){
        this.setState({endDate: e.target.value});
      }

      setPoints(){
        this.setLocations(this.state.identifier, this.state.startDate, this.state.endDate);
        alert('locations have been fetched');
      }
    
      renderPoints(){
         const pointsCompos = this.state.locations.reduce((acc, point, idx) => {
            acc.push(<MyGreatPlace lat={point.latitude} lng={point.longitude} text={idx} />)  
          return acc;
         }, [])

         return pointsCompos;
      }


      render() {
        return (
          // Important! Always set the container height explicitly
          <div>
         
          <div style={{'zIndex' : 2}}>
            <h1>params</h1>
            identifier: <input type='text' name='identifier' placeholder='identifier' onChange={this.handleInput}/> <br/>
            startDate: <input type='date' name='startDate' placeholder='startDate' onChange={this.handleStartDate}/> <br/>
            endDate: <input type='date' name='endDate' placeholder='identifier' onChange={this.handleEndDate}/> <br/>
            
            <button onClick={this.setPoints}>submit</button>
          </div>
         
          <div style={{ height: '100vh', width: '100%' }}>
            <GoogleMapReact
              bootstrapURLKeys={{ key: apiKey }}
              defaultCenter={this.props.center}
              defaultZoom={this.props.zoom}
            >
            {this.renderPoints()}
            </GoogleMapReact>
          </div>
          </div>
          
        );
      }
}