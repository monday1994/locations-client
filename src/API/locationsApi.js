import fetch from 'node-fetch';

const getLocationsInRange = (identifier, startDate, endDate) => {
   return fetch(`http://localhost:3000/locations-in-range?identifier=${identifier}&startDate=${startDate}&endDate=${endDate}`);
};

export { getLocationsInRange }; 